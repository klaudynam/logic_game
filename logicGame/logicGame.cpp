// logicGame.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include "pch.h"
#include <iostream>
#include<ctime>
using namespace std;

void PrintIntroduction(int LevelDifficulty) {
	cout << "You're a secret agent breaking into a " << LevelDifficulty <<" secure server room!\n";
	cout << "You need to enter the correct codes to continue...\n";
}

bool Play(int LevelDifficulty) {


	PrintIntroduction(LevelDifficulty);
	int CodeA = rand()% LevelDifficulty+ LevelDifficulty;
	int CodeB = rand()% LevelDifficulty+ LevelDifficulty;
	int CodeC = rand()% LevelDifficulty+ LevelDifficulty;

	int CodeSum = CodeA + CodeB + CodeC;
	int CodeProduct = CodeA * CodeB * CodeC;


	cout << "There are 3 numbers in the code\n";
	cout << "\nNumbers sum to: " << CodeSum;
	cout << "\nNumbers multiply to: " << CodeProduct;

	int PlayerGuess1, PlayerGuess2, PlayerGuess3;

	cout << "\nEnter your 1st number: ";
	cin >> PlayerGuess1;
	cout << "\nEnter your 2nd number: ";
	cin >> PlayerGuess2;
	cout << "\nEnter your 3rd number: ";
	cin >> PlayerGuess3;

	cout << "Your guess: " << PlayerGuess1 << ", " << PlayerGuess2 << ", " << PlayerGuess3 << ", ";

	int GuessSum = PlayerGuess1 + PlayerGuess2 + PlayerGuess3;
	int GuessProduct = PlayerGuess1 * PlayerGuess2 * PlayerGuess3;

	if (GuessSum == CodeSum && GuessProduct == CodeProduct) {
		cout << "\nYou compelted level " << LevelDifficulty << " !!!";
		return true;
	}
	else
	{
		cout << "\nYou lose!";
		return false;
	}
}


int main()
{
	srand(time(NULL));
	int LevelDifficulty =  1;
	int const MaxLevelDifficulty = 5;
	while (LevelDifficulty <= MaxLevelDifficulty) {
		bool bLevelCompleted = Play(LevelDifficulty);
		cin.clear();
		cin.ignore();

		if (bLevelCompleted) {
			LevelDifficulty++;
		}
	}

	cout << "\nYou won the game!";

	return 0;
}

